
const convertHTMLToPDF = require('pdf-puppeteer');
const fs = require('fs')

// https://logo.clearbit.com/benefitgames.com

const run = async () => {
    convertHTMLToPDF(
        `<style>* {
          margin: 0;
          padding: 0;
          font-family: 'arial';
          }
          .screen {
          width: 100vw;
          height: 100vh;
          margin: 0;
          margin-top: 15px;
          padding: 0px;
          display: flex;
          align-items: top;
          justify-content: center;
          }
          html {
            -webkit-print-color-adjust: exact;
          }
          .receipt {
          margin: 0;
          padding: 0;
          width: 72%;
          background: #e9e9e9;
          height: min-content;
          min-width: 250px;
          max-width: 550px;
          }
          .logoContainer {
          text-align: center;
          margin-top: 15px;
          }
          .thankyou {
          text-align: center;
          margin-top: 30px;
          }
          .orderdetails {
          margin-top: 60px;
          padding-left: 20px;
          padding-right: 20px;
          }
          .orderItems {
          padding-left: 5px;
          padding-right: 5px;
          padding-top: 15px;
          }
          .item {
          padding-bottom: 15px;
          padding-right: 40px;
          display: flex;
          }
          .campaign {
          margin-top: 10px;
          background: white;
          border: 1px solid lightgray;
          box-shadow: 0px 0px 3px 1px rgba(0, 0, 0, 0.1);
          margin-bottom: 20px;
          }
          .campaignData {
          padding-bottom: 5px;
          padding-top: 15px;
          margin-left: 5px;
          margin-right: 5px;
          border-bottom: 2px solid #f6f6f6;
          }
          .fee {
          border-top: 2px solid #f6f6f6;
          padding-top: 15px;
          display: flex;
          padding-right: 40px;
          }
          .tax {
          display: flex;
          margin-bottom: 10px;
          margin-top: 10px;
          padding-right: 40px;
          }
          .total {
          display: flex;
          margin-bottom: 30px;
          padding-right: 40px;
          }
          .paymentdetails {
          padding-left: 20px;
          padding-right: 20px;
          margin-bottom: 10px;
          }
          .paymentdetailscontainer {
          background: white;
          display: flex;
          height: max-content;
          padding: 7px;
          margin-left: 20px;
          margin-right: 20px;
          margin-top: 10px;
          margin-bottom: 20px;
          padding-top: 15px;
          padding-right: 20px;
          border: 1px solid lightgray;
          box-shadow: 0px 0px 3px 1px rgba(0, 0, 0, 0.1);
          }
          .billinfo {
          display: flex;
          flex-direction: column;
          flex: 1;
          }
          .sponsors {
          padding-left: 20px;
          padding-right: 20px;
          margin-top: 40px;
          margin-bottom: 40px;
          }
          .sponsor {
          display: flex;
          margin-bottom: 20px;
          }
          .sponsorimage {
          width: 47.5%;
          margin-right: 2.5%;
          }
          .sponsorinfo {
          width: 47.5%;
          margin-left: 2.5%;
          }
          .termandconditions {
          padding-left: 20px;
          padding-right: 20px;
          margin-bottom: 70px;
          }
          .footer {
          display: flex;
          height: 80px;
          padding-left: 10%;
          padding-right: 5%;
          background: black;
          align-items: center;
          }
          .h1 {
          font-size: 1.4rem;
          font-weight: normal;
          }
          .h2 {
          font-size: 1.3rem;
          font-weight: normal;
          }
          .h3 {
          font-size: 1.1rem;
          font-weight: normal;
          }
          .h4 {
          font-size: 0.9rem;
          font-weight: normal;
          }
          .h5 {
          font-size: 0.7rem;
          font-weight: normal;
          }
          .h6 {
          font-size: 0.6rem;
          font-weight: normal;
          }
          .p {
          font-size: 0.5rem;
          font-weight: normal;
          }
          .subp {
          font-size: 0.4rem;
          font-weight: normal;
          }</style><div class="screen"><div class="receipt"><div class="logoContainer"><img class="logo" width="15%" src="https://logo.clearbit.com/benefitgames.com"/></div><div class="thankyou"><h1 class="h1">THANK YOU</h1></div><div class="orderdetails"><h4 class="h4">ORDER DETAILS</h4><div class="campaign"><div class="banner"><img class="campaignbanner" width="100%" src=""/></div><div class="campaignData"><h4 class="h4" style="font-weight: bold;">Tete FTW Updated 8</h4><h6 class="h6" style="                  color: gray;                  font-weight: bold;                  margin-top: 2px;                  margin-bottom: 7px;                  ">Supporting FrancoOrg++</h6><h5 class="h5" style="color: gray; font-weight: bold;">ORDER #724c1</h5></div><div class="orderItems"><div class="item"><h5 class="h5" style="width: 45px;">3</h5><h5 class="h5" style="display: flex; flex: 1; padding-right: 5px;">General Admision</h5><h5 class="h5" style="font-weight: bold; width: 40px;">$15</h5></div><div class="fee"><h5 class="h5" style="display: flex; flex: 1; justify-content: flex-end; padding-right: 50px;">Fees</h5><h5 class="h5" style="font-weight: bold; width: 40px;">$10</h5></div><div class="tax"><h5 class="h5" style="display: flex; flex: 1; justify-content: flex-end; padding-right: 58px;">Tax</h5><h5 class="h5" style="font-weight: bold; width: 40px;">$0</h5></div><div class="total"><h5 class="h5" style="font-weight: bold; display: flex; flex: 1; justify-content: flex-end; padding-right: 48px;">Total</h5><h5 class="h5" style="font-weight: bold; width: 40px;">$25</h5></div></div></div><div class="campaign"><div class="banner"><img class="campaignbanner" width="100%" src=""/></div><div class="campaignData"><h4 class="h4" style="font-weight: bold;">Tete FTW Updated 8</h4><h6 class="h6" style="                  color: gray;                  font-weight: bold;                  margin-top: 2px;                  margin-bottom: 7px;                  ">Supporting FrancoOrg++</h6><h5 class="h5" style="color: gray; font-weight: bold;">ORDER #724c1</h5></div><div class="orderItems"><div class="item"><h5 class="h5" style="display: flex; flex: 1; padding-left: 45px; padding-right: 5px;">Donation - Daniel Fundraiser test</h5><h5 class="h5" style="font-weight: bold; width: 40px;">$4</h5></div><div class="fee"><h5 class="h5" style="display: flex; flex: 1; justify-content: flex-end; padding-right: 50px;">Fees</h5><h5 class="h5" style="font-weight: bold; width: 40px;">$10</h5></div><div class="tax"><h5 class="h5" style="display: flex; flex: 1; justify-content: flex-end; padding-right: 58px;">Tax</h5><h5 class="h5" style="font-weight: bold; width: 40px;">$0</h5></div><div class="total"><h5 class="h5" style="font-weight: bold; display: flex; flex: 1; justify-content: flex-end; padding-right: 48px;">Total</h5><h5 class="h5" style="font-weight: bold; width: 40px;">$14</h5></div></div></div></div><div class="paymentdetails"><h4 class="h4">PAYMENT DETAILS</h4></div><div class="paymentdetailscontainer"><div class="billinfo"><h5 class="h5" style="font-weight: bold; margin-bottom: 7px;">Bill to:</h5><h5 class="h5">Franco Jimenez</h5><h5 class="h5">dadwadad</h5><h5 class="h5">waeaesd, undefined 87876</h5></div><div class="totalandcarddetails"><div style="display: flex;"><h5 class="h5" style="margin-right: 30px;">Order Total</h5><h5 class="h5" style="font-weight: bold;">$10000</h5></div><div style="margin-top: 35px; padding-bottom: 25px; display: flex; justify-content: flex-end;"><h5 class="h5" style="margin-right: 5px; font-weight: bold;">VISA</h5></div></div></div><div class="footer"><div><img class="logo" width="100%" style="max-width: 70px; min-width: 30px;" src="https://logo.clearbit.com/benefitgames.com"/></div><div style="display: flex; flex-direction: column; flex: 1;"><h5 class="h5" style="color: #FEC511; text-align: center; margin-bottom: 15px;">Host your own game in minutes.</h5><h4 class="h4" style="color: white; text-align: center;">BenefitGames.com</h4></div></div></div></div>`,
        pdf => {
            fs.writeFileSync("./example.pdf", pdf);
        },
        null,
        {args: ["--no-sandbox"]},
        true
    ).catch(err => {
        console.log(err);
    })
    convertHTMLToPDF(
      `<style>* {
        margin: 0;
        padding: 0;
        font-family: "arial";
        }
        .screen {
        width: 100vw;
        height: 100vh;
        margin: 0;
        margin-top: 15px;
        padding: 0px;
        display: flex;
        align-items: top;
        justify-content: center;
        }
        .receipt {
        margin: 0;
        padding: 0;
        width: 72%;
        background: #e9e9e9;
        height: min-content;
        min-width: 250px;
        max-width: 550px;
        }
        .logoContainer {
        text-align: center;
        margin-top: 15px;
        }
        .thankyou {
        text-align: center;
        margin-top: 30px;
        }
        .orderdetails {
        margin-top: 10px;
        padding-left: 20px;
        padding-right: 20px;
        }
        .orderItems {
        padding-left: 5px;
        padding-right: 5px;
        padding-top: 15px;
        }
        .item {
        padding-bottom: 15px;
        padding-right: 40px;
        display: flex;
        }
        .campaign {
        margin-top: 10px;
        background: white;
        border: 1px solid lightgray;
        box-shadow: 0px 0px 3px 1px rgba(0, 0, 0, 0.1);
        margin-bottom: 20px;
        }
        .campaignData {
        padding-bottom: 5px;
        margin-top: 20px;
        padding-top: 15px;
        padding-bottom: 15px;
        margin-left: 5px;
        margin-right: 5px;
        padding-left: 10px;
        padding-right: 10px;
        border-top: 1px solid #f6f6f6;
        border-bottom: 1px solid #f6f6f6;
        }
        .fee {
        border-top: 2px solid #f6f6f6;
        padding-top: 15px;
        display: flex;
        padding-right: 40px;
        }
        .total {
        display: flex;
        margin-bottom: 30px;
        padding-right: 40px;
        margin-top: 10px;
        }
        .paymentdetails {
        padding-left: 20px;
        padding-right: 20px;
        margin-bottom: 10px;
        }
        .totalandcarddetails {
        display: flex;
        flex: 1;
        }
        .paymentdetailscontainer {
        display: flex;
        height: max-content;
        margin-top: 10px;
        padding-left: 15px;
        padding-top: 20px;
        padding-right: 15px;
        }
        .paymentdetailscontainer2 {
        background: white;
        display: flex;
        height: max-content;
        padding: 7px;
        margin-left: 20px;
        margin-right: 20px;
        margin-top: 10px;
        margin-bottom: 20px;
        padding-top: 20px;
        padding-bottom: 20px;
        padding-left: 15px;
        padding-right: 35px;
        border: 1px solid lightgray;
        box-shadow: 0px 0px 3px 1px rgba(0, 0, 0, 0.1);
        }
        .billinfo {
        display: flex;
        flex-direction: column;
        flex: 1;
        }
        .sponsors {
        padding-left: 20px;
        padding-right: 20px;
        margin-top: 40px;
        margin-bottom: 40px;
        }
        .sponsor {
        display: flex;
        margin-bottom: 20px;
        }
        .sponsorimage {
        width: 47.5%;
        margin-right: 2.5%;
        }
        .sponsorinfo {
        width: 47.5%;
        margin-left: 2.5%;
        }
        .termandconditions {
        padding-left: 20px;
        padding-right: 20px;
        margin-bottom: 70px;
        }
        .footer {
        display: flex;
        height: 80px;
        padding-left: 10%;
        padding-right: 5%;
        background: black;
        align-items: center;
        }
        .h1 {
        font-size: 1.4rem;
        font-weight: normal;
        }
        .h2 {
        font-size: 1.3rem;
        font-weight: normal;
        }
        .h3 {
        font-size: 1.1rem;
        font-weight: normal;
        }
        .h4 {
        font-size: 0.9rem;
        font-weight: normal;
        }
        .h5 {
        font-size: 0.7rem;
        font-weight: normal;
        }
        .h6 {
        font-size: 0.6rem;
        font-weight: normal;
        }
        .p {
        font-size: 0.5rem;
        font-weight: normal;
        }
        .subp {
        font-size: 0.4rem;
        font-weight: normal;
        }</style><div class="screen"><div class="receipt"><div class="logoContainer"><img class="logo" width="15%" src="https://logo.clearbit.com/benefitgames.com"/></div><div class="orderdetails"><div class="campaign"><h2 style="text-align: center; margin-top: 30px;">Thank you for your donation</h2><div class="paymentdetailscontainer"><div class="billinfo"><div style="display: flex;"><h5 class="h5" style="font-weight: bold; margin-bottom: 7px; flex: 1;">Bill to:</h5><h5 class="h5" style="color: gray; font-weight: bold;">ORDER #724c1</h5></div><h5 class="h5">Franco Jimenez</h5><h5 class="h5">dadwadad</h5><h5 class="h5">waeaesd, undefined 87876</h5></div></div><div class="campaignData"><h4 class="h4" style="font-weight: bold;">Tete FTW Updated 8</h4><div style="display: flex; align-items: center; margin-top: 5px;"><div style="margin-right: 10px;"><img class="logo" width="100%" style="max-width: 40px; min-width: 20px;" src=""/></div><div style="padding-top: 2px;"><h6 class="h6" style="                                color: gray;                                font-weight: bold;                                margin-top: 2px;                                margin-bottom: 7px;                                ">Supporting FrancoOrg++</h6><h5 class="h5" style="color: gray; font-weight: bold;">EIN # 5555</h5></div></div></div><div class="orderItems"></div></div><div class="campaign"><h2 style="text-align: center; margin-top: 30px;">Thank you for your donation</h2><div class="paymentdetailscontainer"><div class="billinfo"><div style="display: flex;"><h5 class="h5" style="font-weight: bold; margin-bottom: 7px; flex: 1;">Bill to:</h5><h5 class="h5" style="color: gray; font-weight: bold;">ORDER #724c1</h5></div><h5 class="h5">Franco Jimenez</h5><h5 class="h5">dadwadad</h5><h5 class="h5">waeaesd, undefined 87876</h5></div></div><div class="campaignData"><h4 class="h4" style="font-weight: bold;">Tete FTW Updated 8</h4><div style="display: flex; align-items: center; margin-top: 5px;"><div style="margin-right: 10px;"><img class="logo" width="100%" style="max-width: 40px; min-width: 20px;" src=""/></div><div style="padding-top: 2px;"><h6 class="h6" style="                                color: gray;                                font-weight: bold;                                margin-top: 2px;                                margin-bottom: 7px;                                ">Supporting FrancoOrg++</h6><h5 class="h5" style="color: gray; font-weight: bold;">EIN # 5555</h5></div></div></div><div class="orderItems"><div class="item"><h5 class="h5" style="                            display: flex;                            flex: 1;                            padding-left: 45px;                            padding-right: 5px;                            ">Donation - Daniel Fundraiser test</h5><h5 class="h5" style="font-weight: bold; width: 40px;">$4</h5></div><div class="fee"><h5 class="h5" style="                            display: flex;                            flex: 1;                            justify-content: flex-end;                            padding-right: 50px;                            ">Fees</h5><h5 class="h5" style="font-weight: bold; width: 40px;">$10</h5></div><div class="total"><h5 class="h5" style="                            font-weight: bold;                            display: flex;                            flex: 1;                            justify-content: flex-end;                            padding-right: 49px;                            ">Total</h5><h5 class="h5" style="font-weight: bold; width: 40px;">$4</h5></div></div></div></div><div class="paymentdetails"><h4 class="h4">PAYMENT DETAILS</h4></div><div class="paymentdetailscontainer2"><div class="totalandcarddetails"><div style="display: flex; flex: 1;"><h5 class="h5" style="margin-right: 10px; font-weight: bold;">VISA</h5></div><div style="display: flex;"><h5 class="h5" style="font-weight: bold;">$10000</h5></div></div></div><div class="footer"><div><img class="logo" width="100%" style="max-width: 70px; min-width: 30px;" src="https://logo.clearbit.com/benefitgames.com"/></div><div style="display: flex; flex-direction: column; flex: 1;"><h5 class="h5" style="color: #fec511; text-align: center; margin-bottom: 15px;">Host your own game in minutes.</h5><h4 class="h4" style="color: white; text-align: center;">BenefitGames.com</h4></div></div></div></div>`,
      pdf => {
          fs.writeFileSync("./example2.pdf", pdf);
      },
      null,
      {args: ["--no-sandbox"]},
      true
  ).catch(err => {
      console.log(err);
  })

  convertHTMLToPDF(
    `<style>* {
      margin: 0;
      padding: 0;
      font-family: 'arial';
      }
      .screen {
      width: 100vw;
      height: 100vh;
      margin: 0;
      margin-top: 15px;
      padding: 0px;
      display: flex;
      align-items: top;
      justify-content: center;
      }
      .receipt {
      margin: 0;
      padding: 0;
      width: 72%;
      background: #e9e9e9;
      height: min-content;
      min-width: 250px;
      max-width: 550px;
      }
      .logoContainer {
      text-align: center;
      margin-top: 15px;
      }
      .thisisticket {
      text-align: center;
      margin-top: 15px;
      }
      .orderdetails {
      padding-left: 20px;
      padding-right: 20px;
      }
      .locationandaddress {
      padding-left: 5px;
      padding-right: 5px;
      padding-top: 10px;
      padding-bottom: 10px;
      border-bottom: 1px solid #f6f6f6;
      }
      .campaign {
      background: white;
      border: 1px solid lightgray;
      box-shadow: 0px 0px 3px 1px rgba(0, 0, 0, 0.1);
      margin-bottom: 20px;
      }
      .campaignData {
      padding-bottom: 5px;
      padding-top: 5px;
      margin-left: 5px;
      margin-right: 5px;
      border-bottom: 2px solid #f6f6f6;
      }
      .sponsors {
      padding-left: 20px;
      padding-right: 20px;
      margin-top: 40px;
      margin-bottom: 40px;
      }
      .sponsor {
      display: flex;
      margin-bottom: 20px;
      }
      .sponsorimage {
      width: 47.5%;
      margin-right: 2.5%;
      }
      .sponsorinfo {
      width: 47.5%;
      margin-left: 2.5%;
      }
      .termandconditions {
      padding-left: 20px;
      padding-right: 20px;
      margin-bottom: 70px;
      }
      .footer {
      display: flex;
      height: 80px;
      padding-left: 10%;
      padding-right: 5%;
      background: black;
      align-items: center;
      }
      .h1 {
      font-size: 1.4rem;
      font-weight: normal;
      }
      .h2 {
      font-size: 1.3rem;
      font-weight: normal;
      }
      .h3 {
      font-size: 1.1rem;
      font-weight: normal;
      }
      .h4 {
      font-size: 0.9rem;
      font-weight: normal;
      }
      .h5 {
      font-size: 0.7rem;
      font-weight: normal;
      }
      .h6 {
      font-size: 0.6rem;
      font-weight: normal;
      }
      .p {
      font-size: 0.5rem;
      font-weight: normal;
      }
      .subp {
      font-size: 0.4rem;
      font-weight: normal;
      }
      .codebar {
      padding-top: 10px;
      padding-bottom: 10px;
      display: flex;
      flex-direction: column;
      justify-content: center;
      align-items: center;
      text-align: center;
      }
      .info {
      display: flex;
      padding-left: 10px;
      padding-right: 15px;
      padding-top: 10px;
      padding-bottom: 10px;
      }</style><div class="screen"><div class="receipt"><div class="logoContainer"><img class="logo" width="20%" src="https://via.placeholder.com/200?text=Logo"/></div><div class="thisisticket"><h4 class="h4" style="font-weight: bold;">This is your ticket</h4><h5 class="h5" style="margin-top: 5px; font-weight: bold; color: gray;">ORDER #724c1</h5></div><div class="orderdetails"><div style="height: 25px; width: 100%; display: flex; justify-content: center; align-items: center;"><div style="width: 50px; height: 25px; background: #e9e9e9; border-radius: 50%; position: relative; bottom: -12px; box-shadow: inset 0px -2px 0px 0px rgba(0, 0, 0, 0.05);; border: 1px solid #e9e9e9;"></div></div><div class="campaign"><div class="info"><div style="display: flex; flex: 1;"><h5 class="h5">General Admision</h5></div><div style="display: flex; flex-direction: column; align-items: center;"><h5 class="h5">Wed Apr 01 2020 04:54:54 GMT+0000 (Coordinated Universal Time)</h5><h5 class="h5"></h5></div></div><div class="banner"><img class="campaignbanner" width="100%" src=""/></div><div class="campaignData"><h4 class="h4" style="font-weight: bold;">Tete FTW Updated 8</h4><h6 class="h6" style="          color: gray;          font-weight: bold;          margin-top: 2px;          margin-bottom: 7px;          ">Supporting FrancoOrg++</h6></div><div class="locationandaddress"><h5 class="h5" style="color: gray; font-weight: bold;">12345 Great Street Ave, XY 12345</h5><h6 class="h6" style="color: gray; font-weight: bold;">12345 Great Street Ave, XY 12345</h6></div><div class="codebar"><h6 class="h6" style="margin-bottom: 5px;">General Admision</h6><h6 class="h6" style="font-weight: bold; margin-top: 5px;">5e950b551b7b8d00198724c4</h6></div></div></div><div class="footer"><div><img class="logo" width="100%" style="max-width: 70px; min-width: 30px;" src="https://logo.clearbit.com/benefitgames.com"/></div><div style="display: flex; flex-direction: column; flex: 1;"><h5 class="h5" style="color: #FEC511; text-align: center; margin-bottom: 15px;">Host your own game in minutes.</h5><h4 class="h4" style="color: white; text-align: center;">BenefitGames.com</h4></div></div></div></div>`
      ,
    pdf => {
        fs.writeFileSync("./example3.pdf", pdf);
    },
    null,
    {args: ["--no-sandbox"]},
    true
).catch(err => {
    console.log(err);
})
};


// Start the server.
try {
  run()
} catch (error) {
  console.error(error)
}