var fs = require('fs')
var conversion = require("phantom-html-to-pdf")();
conversion({ html: `<style>* {
  margin: 0;
  padding: 0;
  }
  .screen {
  width: 100vw;
  height: 100vh;
  margin: 0;
  margin-top: 15px;
  padding: 0px;
  display: flex;
  align-items: top;
  justify-content: center;
  }
  .navbar {
  display: flex;
  }
  .logoDiv {
  flex: 1;
  display: flex;
  align-items: center;
  }
  .logo {
  width: 100%;
  max-width: 80px;
  min-width: 40px;
  }
  .orderId {
  display: flex;
  align-items: center;
  }
  .content {
  margin: 0;
  padding: 0;
  width: 55%;
  }
  .hiUser {
  margin-top: 25px;
  width: 100%;
  }
  ul {
  list-style: none;
  }
  .tickets {
  margin-top: 30px;
  width: 100%;
  }
  .donations {
  margin-top: 20px;
  width: 100%;
  }
  .total {
  width: 100%;
  display: flex;
  justify-content: flex-end;
  margin-top: 10px;
  }</style><div class="screen"><div class="content"><div class="navbar"><div class="logoDiv"><img class="logo" src="https://logo.clearbit.com/benefitgames.com"/><h3 style="margin-left: 5px;">Benefit Games</h3></div><div class="orderId"><p>5e8e017f3faffb00194a4c6d</p></div></div><div class="hiuser"><p>Thank you Franco Jimenez ! Here is a detailed receipt of your order made Wed Apr 08 2020 16:53:19 GMT+0000 (Coordinated Universal Time):</p></div><div class="tickets"><h4 style="margin-bottom: 5px;">Tickets bought:</h4><li>General Admision 5e8e017f3faffb00194a4c6e $5</li><li>General Admision 5e8e017f3faffb00194a4c70 $5</li><li>General Admision 5e8e017f3faffb00194a4c72 $5</li><li>General Admision 5e8e017f3faffb00194a4c71 $5</li><li>General Admision 5e8e017f3faffb00194a4c6f $5</li></div><div class="donations"><h4 style="margin-bottom: 5px;">Thank you for your donations!</h4><li>5e8e01743faffb00194a4c0b $4</li></div><div class="total"><p>Total: 80</p></div></div></div>
  ` }, function(err, pdf) {
    var output = fs.createWriteStream('./example.pdf')
  console.log(pdf);
  // console.log(pdf.numberOfPages);
  //   // since pdf.stream is a node.js stream you can use it
    // to save the pdf to a file (like in this example) or to
    // respond an http request.
  pdf.stream.pipe(output);
});

// const run = async () => {
//     convertHTMLToPDF(
//         ,
//         pdf => {
//             fs.writeFileSync("./example.pdf", pdf);
//         },
//         null,
//         {args: ["--no-sandbox"]},
//         true
//     ).catch(err => {
//         console.log(err);
//     })
// };


// // Start the server.
// try {
//   run()
// } catch (error) {
//   console.error(error)
// }